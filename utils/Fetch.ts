import Axios from 'axios'

export const FetchFromGenshinDev = Axios.create({
  baseURL: 'https://api.genshin.dev/',
})
