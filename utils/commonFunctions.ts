export const percent = (number: number) => {
  return number ? number / 100 : 0
}

export const incPercent = (number: number) => {
  return number ? 1 + number / 100 : 1
}
