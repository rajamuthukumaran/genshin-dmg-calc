import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { Provider } from 'react-redux'
import Store from '../redux/store'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={Store}>
      <div className="min-h-screen  bg-black py-2 text-white">
        <Component {...pageProps} />
      </div>
    </Provider>
  )
}

export default MyApp
