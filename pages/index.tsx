import type { NextPage } from 'next'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import { percent, incPercent } from '../utils/commonFunctions'

const fields = [
  {
    name: 'attack',
    label: 'Attack',
  },
  {
    name: 'skillDmg',
    label: 'Skill Damage',
  },
  {
    name: 'elementalDmg',
    label: 'Elemental Damage',
  },
  {
    name: 'resistance',
    label: 'Resistance',
  },
]

export type Values = {
  attack: number
  skillDmg: number
  elementalDmg: number
  resistance: number
}

const Home: NextPage = () => {
  const [sum, setSum] = useState(0)
  const [values, setValues] = useState<Values>({
    attack: 0,
    skillDmg: 0,
    elementalDmg: 0,
    resistance: 0,
  })

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target

    setValues((prev) => ({
      ...prev,
      [name]: value,
    }))
  }

  const calcuation = () => {
    const { attack, elementalDmg, resistance, skillDmg } = values

    const dmg =
      attack *
      percent(skillDmg) *
      incPercent(elementalDmg) *
      incPercent(resistance)

    setSum(dmg || 0)
  }

  useEffect(() => {
    calcuation()
  }, [values])

  return (
    <div>
      <Head>
        <title>Genshin Damage Calculator</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <header className="py-10 px-10 md:py-20">
        <h1 className="text-center text-3xl font-bold text-purple-800 md:text-6xl">
          Genshin Damage Calculator
        </h1>
      </header>

      <main className="flex w-full flex-1 flex-col items-center px-20 md:justify-center md:text-center">
        <div>
          <form>
            {fields.map((data) => (
              <div
                key={data.name}
                className="mb-8 flex flex-col md:mb-2 md:flex-row md:justify-between"
              >
                <label
                  className="text-xl font-medium text-purple-500"
                  htmlFor="attack"
                >
                  {data.label}
                </label>
                <input
                  className="border-b-2 border-b-purple-500 bg-black text-xl text-white outline-none md:mx-5"
                  type="number"
                  name={data.name}
                  onChange={handleChange}
                />
              </div>
            ))}
          </form>

          <div className="mt-10 text-left md:mt-8">
            {!!sum && (
              <p className="text-bold text-2xl text-purple-500">
                Estimated damage output is {sum}
              </p>
            )}
          </div>
        </div>
      </main>
    </div>
  )
}

export default Home
