import { useRouter } from 'next/router'
import { useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getCharacterDetails } from '../../../redux/actions/character'
import { Character, Characters } from '../../../redux/type'
import moment from 'moment'

const Character = () => {
  const dispatch = useDispatch()
  const router = useRouter()

  const character = useMemo(() => {
    return router.query?.character
  }, [router.query?.character])

  const info: Character = useSelector((state: any) => state?.character)

  useEffect(() => {
    dispatch(getCharacterDetails(character as Characters))
  }, [character])

  return (
    <div className="p-10">
      <div>
        <h1 className="text-5xl">{info.name}</h1>
        <Rarity star={info.rarity} />
      </div>

      <div className="my-10">
        <Entry
          label="Birthday"
          value={moment(info.birthday).format('DD MMM YYYY')}
        />
        <Entry label="Nation" value={info.nation} />
        <Entry label="Weapon" value={info.weapon} />
      </div>

      <p>{info.description}</p>
    </div>
  )
}

const Entry: React.FC<{ label: string; value: string }> = ({
  label,
  value,
}) => {
  return (
    <div className="my-2 flex justify-between text-xl md:block">
      <span className="text-purple-400 md:inline-block md:w-[100px]">
        {label}:{' '}
      </span>
      <span>{value}</span>
    </div>
  )
}

const Rarity: React.FC<{ star: number }> = ({ star }) => {
  return (
    <span>
      {[...Array(star)].map((i, index) => (
        <span key={index} className="text-3xl text-yellow-400">
          &#9733;
        </span>
      ))}
    </span>
  )
}

export default Character
