// -------------------- characters -------------------------------------

export interface Character {
  id: number
  name: string
  slug: string
  description: string
  gender: Gender
  birthday: string
  rarity: number
  vision: string
  weapon: Weapon
  obtain: Obtain
  nation: string
}

export enum Gender {
  Female = 'female',
  Male = 'male',
}

export enum Obtain {
  Quest = 'Quest',
  Unknown = 'Unknown',
  Wish = 'Wish',
}

export enum Weapon {
  Bow = 'bow',
  Catalyst = 'catalyst',
  Claymore = 'claymore',
  Polearm = 'polearm',
  Sword = 'sword',
}

export enum Characters {
  'albedo' = 'albedo',
  'aloy' = 'aloy',
  'amber' = 'amber',
  'arataki-itto' = 'arataki-itto',
  'ayaka' = 'ayaka',
  'barbara' = 'barbara',
  'beidou' = 'beidou',
  'bennett' = 'bennett',
  'chongyun' = 'chongyun',
  'diluc' = 'diluc',
  'diona' = 'diona',
  'eula' = 'eula',
  'fischl' = 'fischl',
  'ganyu' = 'ganyu',
  'gorou' = 'gorou',
  'hu-tao' = 'hu-tao',
  'jean' = 'jean',
  'kaeya' = 'kaeya',
  'kazuha' = 'kazuha',
  'keqing' = 'keqing',
  'klee' = 'klee',
  'kokomi' = 'kokomi',
  'lisa' = 'lisa',
  'mona' = 'mona',
  'ningguang' = 'ningguang',
  'noelle' = 'noelle',
  'qiqi' = 'qiqi',
  'raiden' = 'raiden',
  'razor' = 'razor',
  'rosaria' = 'rosaria',
  'sara' = 'sara',
  'sayu' = 'sayu',
  'shenhe' = 'shenhe',
  'sucrose' = 'sucrose',
  'tartaglia' = 'tartaglia',
  'thoma' = 'thoma',
  'traveler-anemo' = 'traveler-anemo',
  'traveler-electro' = 'traveler-electro',
  'traveler-geo' = 'traveler-geo',
  'venti' = 'venti',
  'xiangling' = 'xiangling',
  'xiao' = 'xiao',
  'xingqiu' = 'xingqiu',
  'xinyan' = 'xinyan',
  'yanfei' = 'yanfei',
  'yoimiya' = 'yoimiya',
  'yun-jin' = 'yun-jin',
  'zhongli' = 'zhongli',
}
