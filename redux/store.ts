import { applyMiddleware, createStore } from 'redux'
import { reducers } from './reducers'
import { watcherSaga } from './saga/rootSage'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension'

const sagaMiddleware = createSagaMiddleware()

const middlewares = [sagaMiddleware]

export const Store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(...middlewares))
)

sagaMiddleware.run(watcherSaga)

export default Store
