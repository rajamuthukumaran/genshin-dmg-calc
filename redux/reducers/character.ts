import ACTION, { Action } from '../actions'
import { Character } from '../type'

export type CharacterReducer = Character | {}

const initState = {}

export const character = (state: CharacterReducer, action: Action) => {
  switch (action.type) {
    case ACTION.SET_CHARACTER:
      return action.payload

    case ACTION.RESET:
      return initState

    default:
      return initState
  }
}
