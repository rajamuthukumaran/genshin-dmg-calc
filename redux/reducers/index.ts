import { combineReducers } from 'redux'
import { character } from './character'

export const reducers = combineReducers({ character })
