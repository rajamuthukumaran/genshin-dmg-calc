export enum ACTION {
  'RESET' = 'RESET',
  'GET_CHARACTER' = 'GET_CHARACTER',
  'SET_CHARACTER' = 'SET_CHARACTER',
}

export type Action = {
  type: ACTION
  payload?: any
}

export default ACTION
