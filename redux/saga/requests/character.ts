import { FetchFromGenshinDev } from '../../../utils/Fetch'
import { Character, Characters } from '../../type'

export const getCharacterDetails = (
  character: Characters = Characters.eula
) => {
  return FetchFromGenshinDev.get<Character>(`characters/${character}`)
}
