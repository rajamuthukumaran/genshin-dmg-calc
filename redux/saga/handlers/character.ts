import { AxiosResponse } from 'axios'
import { call, put } from 'redux-saga/effects'
import { Action } from '../../actions'
import { setCharacterDetails } from '../../actions/character'
import { Character } from '../../type'
import { getCharacterDetails } from '../requests/character'

export function* handleGetCharacterDetails(action: Action) {
  try {
    const res: AxiosResponse<Character> = yield call(() =>
      getCharacterDetails(action.payload)
    )
    yield put(setCharacterDetails(res?.data))
  } catch (err) {
    console.error(err)
  }
}
