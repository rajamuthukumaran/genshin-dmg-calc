import { takeLatest } from 'redux-saga/effects'
import { ACTION } from '../actions'
import { handleGetCharacterDetails } from './handlers/character'

export function* watcherSaga() {
  yield takeLatest(ACTION.GET_CHARACTER, handleGetCharacterDetails)
}
