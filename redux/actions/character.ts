import ACTION from '../actions'
import { Character, Characters } from '../type'

export const getCharacterDetails = (character: Characters) => ({
  type: ACTION.GET_CHARACTER,
  payload: character,
})

export const setCharacterDetails = (characterInfo: Character) => ({
  type: ACTION.SET_CHARACTER,
  payload: characterInfo,
})
